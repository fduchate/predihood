# Contributing to Predihood

The following is a set of guidelines for contributing to this repository.

The Code of Conduct is adapted from the Contributor Covenant [Code of Conduct v2.0](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).

The Contributing sections are inspired from the [Eiffel community guidelines](https://github.com/eiffel-community/.github).

## Code of Conduct 

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

Examples of behavior that contributes to creating a positive environment include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a professional setting

Report to one of the project's maintainer any abuse.

## How to propose changes

Anyone is welcome to propose changes to the contents of this repository by creating a _new issue ticket_ in GitHub. These requests may concern anything contained in the repository: changes to documentation, bug fixes, requests for additional information, additional features, etc.

When posting a new issue, try to be as precise as possible and formulate your arguments for your request carefully. Keep in mind that collaborating on software development is often an exercise in finding workable compromises between multiple and often conflicting needs. In particular, pay attention to the following:
1. What type of change is requested?
2. Why would you like to see this change?
3. Can you provide any concrete examples?
4. Which arguments in favor can you think of?
5. Which arguments against can you think of, and why are they outweighed by the arguments in favor?

Also, keep in mind that just as anyone is welcome to propose a change, anyone is welcome to disagree with and criticize that proposal.

### Closing issues

An issue can be closed by any member of the repository maintainers' team. This can happen in various ways, for varying reasons:
1. Issues without conclusion and no activity for at least 1 month may be closed, as a mere housekeeping activity. For instance, an issue met with requests for further clarification, but left unanswered by the original author, may simply be removed.
2. Issues may simply be rejected if found unfeasible or undesirable. In such cases they shall also be responded to, providing a polite and concise explanation as to why the proposal is rejected.
3. Issues may be closed because they are implemented. Following the successful merging of a pull request addressing an issue, it will be closed.

## How to contribute

While we welcome requests for changes (in the form of issues), we absolutely love ready solutions (in the form of Pull Requests). The best requests are the ones with Pull Requests to go along with them.

Contributions can be made by anyone using the standard [GitHub Fork and Pull model](https://help.github.com/articles/about-pull-requests). When making a pull request, keep a few things in mind.
1. Always explicitly connect a pull request to an issue (as indicated by the issue template).
2. Make sure you target the correct branch. If you are unsure which branch is appropriate, ask in the issue thread.
3. Pull Requests will be publicly reviewed, criticized, and potentially rejected. Don't take it personally.

### Reviewing and merging pull requests

Pull requests can be merged by of the repository maintainers' team.
1. A pull request should be approved by at least two maintainers (including the one doing the merging).
3. When merging, ensure that the description reflects the change. That description should include an issue reference, and should focus on *why* the change was made, to provide the reader with context.

### Closing pull requests

If the author of a pull request has not made any changes or status updates in one month time, any member of the repository maintainers' team should notify the author that they will close the pull request (see below for an example). The member should wait a week after putting the notice before closing the pull request.

  > No one has made an update to this pull request for one month. Please add a status update or we will close this pull request.


