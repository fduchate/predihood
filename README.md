# Predihood

This repository is not used anymore for Predihood (due to restrictions for external collaboration).

Predihood is now at [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood).

## English version

[Predihood](https://gitlab.com/fduchate/predihood) is a web application (Python, JavaScript) for predicting information about neighbourhoods (e.g., environment type, possibility of resting place for migratory birds). The application includes a cartographic interface and a testing interface. Predictive algorithms are based on the [Scikit-learn](https://scikit-learn.org/) library, but new ones can easily be added.

By default, a database of neighbourhoods is provided for France using [IRIS](https://www.insee.fr/fr/metadonnees/definition/c1523) (administrative areas defined by the French institute of statistics, they can be considered as neighbourhoods) and indicators which describe them (e.g. number of bakeries, average income and even the number of houses over 250m^2).
This database contains 50,000 neighbourhoods and 550 indicators (in average) per neighbourhood.
It is hosted on the project [mongiris](https://gitlab.liris.cnrs.fr/fduchate/mongiris).

Predihood is now at [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood).

## Version française

Predihood est une application web (Python, JavaScript) qui permet de réaliser des prédictions sur des quartiers (ex, type d'environnement, possibilité de halte pour des oiseaux migrateurs). L'application inclut une visualisation cartographique et d'une interface de tests. Les algorithmes de prédiction utilisés sont ceux de [Scikit-learn](https://scikit-learn.org/) mais de nouveaux algorithmes peuvent être ajoutés.

Par défaut, une base de quartiers est fournie pour la France en utilisant les [IRIS](https://www.insee.fr/fr/metadonnees/definition/c1523) (unités administratives définies par l'INSEE, pouvant être vues comme des quartiers) et leurs indicateurs (ex, nombre de boulangeries, salaire moyen ou nombre de maisons de plus de 250 m2). Cette base contient 50 000 quartiers et 550 indicateurs (en moyenne) par quartier.
Cette base est hébergé dans le projet [mongiris](https://gitlab.liris.cnrs.fr/fduchate/mongiris).

Ce dépôt ne permettant pas de collaboration externe, la dernière version de Predihood a été déplacée sur [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood).

