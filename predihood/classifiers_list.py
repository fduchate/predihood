"""
Produces the list of available classifiers: 6 from the [Scikit-learn library](https://scikit-learn.org/), and Python files added in `algorithms/`.

Note that if users do not select an algorithm among those available, Random Forest is chosen as a default algorithm (see `utility_functions.py > get_classifier()`).
"""

import sys
import os
import importlib
import re

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier


# define available classifiers for the algorithmic interface
tree = os.listdir('algorithms')
regex = re.compile(r'__[^_]*__')
tree_filtered = [i for i in tree if not regex.match(i) and i.endswith('.py')]  # remove not .py files (eg, __pycache__)
# eternal hack for python imports (here to access parent directory)
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


AVAILABLE_CLASSIFIERS = {
    "Random Forest Classifier": RandomForestClassifier,  # do not change this one, this is the default value if invalid algorithm name
    "KNeighbors Classifier": KNeighborsClassifier,
    "Decision Tree Classifier": DecisionTreeClassifier,
    "SVC": SVC,
    "AdaBoost Classifier": AdaBoostClassifier,
    "MLP Classifier": MLPClassifier
}

for i in tree_filtered:
    name = i[:-3]  # remove .py extension to get only the name of the class
    module = importlib.import_module("predihood.algorithms."+name)
    class_ = getattr(module, name)  # create an object representing the algorithm
    AVAILABLE_CLASSIFIERS[name] = class_
