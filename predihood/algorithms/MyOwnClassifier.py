# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

from sklearn.metrics import euclidean_distances
from sklearn.utils import check_X_y
from sklearn.utils.multiclass import unique_labels
from sklearn.utils.validation import check_is_fitted, check_array

import numpy as np
from predihood.classes.Classifier import Classifier


class MyOwnClassifier(Classifier):
    """Some text.
    Parameters
    ------------
    a : float, default=0.01
        Description of a.
    b : int, default=10
        Description of b.
    """

    def __init__(self, a=0.01, b=10):
        self.a = a
        self.b = b

    def fit(self, X, y):
        # Store the classes seen during fit
        self.classes_ = unique_labels(y)

        self.X_ = X
        self.y_ = y
        
        # Return the classifier
        return self

    def predict(self, df):
        closest = np.argmin(euclidean_distances(df, self.X_), axis=1)
        return self.y_[closest]

    def get_params(self, deep=True):
        # suppose this estimator has parameters "a" and "b"
        return {"a": self.a, "b": self.b}


