class Classifier:
    # def __init__(self):

    def fit(self, X, y):
        raise NotImplementedError("You must implement your own fit function.")

    def predict(self, df):
        raise NotImplementedError("You must implement your own predict function.")

    def get_params(self, deep=True):
        raise NotImplementedError("You must implement your own get_params function.")

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)
        return self
